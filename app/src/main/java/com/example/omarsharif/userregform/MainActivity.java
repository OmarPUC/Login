package com.example.omarsharif.userregform;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    String[] cities = {"Chittagong", "Dhaka", "Comilla", "Shylet", "Khulna", "Barishal", "Cox's Bazar"};
    private EditText editFirstName;
    private EditText editLastName;
    private EditText editPhone;
    private EditText editEmail;
    private EditText editPassword;
    private CheckBox checkBoxDesigner;
    private CheckBox checkBoxProgrammer;
    private Spinner spinnerCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        editFirstName = findViewById(R.id.firstName);
        editLastName = findViewById(R.id.lastName);
        editPhone = findViewById(R.id.phone);
        editEmail = findViewById(R.id.email);
        editPassword = findViewById(R.id.password);


        checkBoxDesigner = findViewById(R.id.checkBox1);
        checkBoxProgrammer = findViewById(R.id.checkBox2);


        spinnerCity = findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, cities);
        spinnerCity.setAdapter(adapter);
    }

    public void radioBtnClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()) {
            case R.id.male:
                if (checked) {
                    Toast.makeText(MainActivity.this, "Male Selected!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.female:
                if (checked) {
                    Toast.makeText(MainActivity.this, "Female Selected!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void sendToNext(View view) {
        String firstname = editFirstName.getText().toString();
        String lastname = editLastName.getText().toString();
        String phone = editPhone.getText().toString();
        String email = editEmail.getText().toString();
        String password = editPassword.getText().toString();

        Intent intent = new Intent(this, Main2Activity.class);

        String fullName = firstname + " " + lastname;

        intent.putExtra("name", fullName);
        intent.putExtra("phone", phone);
        intent.putExtra("email", email);
        intent.putExtra("password", password);

        startActivity(intent);
    }

    public void sendToEmail(View view) {
    }
}
