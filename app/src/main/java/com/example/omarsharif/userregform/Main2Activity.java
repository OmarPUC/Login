package com.example.omarsharif.userregform;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {
    private TextView viewFullName;
    private TextView viewPhone;
    private TextView viewEmail;
    private TextView viewPassword;
    private TextView viewGender;
    private TextView viewProfession;
    private TextView viewCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        viewFullName = findViewById(R.id.fullName);
        viewPhone = findViewById(R.id.phone);
        viewEmail = findViewById(R.id.email);
        viewPassword = findViewById(R.id.password);


        String name = getIntent().getStringExtra("name");
        String phone = getIntent().getStringExtra("phone");
        String email = getIntent().getStringExtra("email");
        String password = getIntent().getStringExtra("password");


        viewFullName.setText(" " + name);
        viewPhone.setText(" " + phone);
        viewEmail.setText(" " + email);
        viewPassword.setText(" " + password);
    }
}
